import copy

class Player:

    batting_stats = ["G","AB","R","H","2B","3B","HR","RBI","BB","SO","SB","CS","HBP","SH","SF"]
    pitching_stats = ["W","L","G","GS","CG","SHO","SV","IPouts","H","ER","HR","BB","SO","IBB","WP","HBP","BK","BFP","GF","R","SH","SF","GIDP"]

    
    def __init__(self, is_hitter):
        self.is_hitter = is_hitter
        self.player_id = ""
        self.name = ""
        self.age = 0
        self.year = 0
        self.stats = {}
        
        if (is_hitter):
            for stat in self.batting_stats:
                self.stats[stat] = 0
        else:
            for stat in self.pitching_stats:
                self.stats[stat] = 0


## Hitter functions

    def get_obp(self):
    
        numerator = self.stats["H"] + self.stats["BB"] + self.stats["HBP"]
        denominator = self.get_pa()
        
        return numerator / denominator
    
    def get_slg(self):
    
        return self.get_tb() / self.stats["AB"]
    
    def get_tb(self):
    
        _1B = self.stats["H"] - self.stats["2B"] - self.stats["3B"] - self.stats["HR"]
        
        return _1B + (self.stats["2B"] * 2) + (self.stats["3B"] * 3) + (self.stats["HR"] * 4)
    
    def get_pa(self):
    
        return self.stats["AB"] + self.stats["BB"] + self.stats["HBP"] + self.stats["SF"] + self.stats["SH"]


## Pitcher functions
    
    def get_era(self):
        return self.stats["ER"] / self.get_ip() * 9
    
    def get_ip(self):
        return self.stats["IPouts"] / 3


## Stat manipulation    
    def add_stats(self, player2):
        
        for stat in self.stats:
            self.stats[stat] = self.stats[stat] + player2.stats[stat]
    
    def multiply_stats(self, weight):

        new_player = copy.deepcopy(self)
        for stat in self.stats:
            new_player.stats[stat] = self.stats[stat] * weight
            
        return new_player
    
    def divide_stats(self, denominator):
    
        new_player = copy.deepcopy(self)
        return new_player.multiply_stats(1 / denominator)
    
    def round_stats(self):
        
        new_player = copy.deepcopy(self)
        for stat in new_player.stats:
            new_player.stats[stat] = round(new_player.stats[stat])
        
        return new_player
    
    def adjust_player_pa(self, new_plate_appearances):
    
        new_player = copy.deepcopy(self)
        current_plate_appearances = new_player.get_pa()
        return new_player.multiply_stats(new_plate_appearances / current_plate_appearances)
    
    
    def adjust_player_bfp(self, new_batters_faced):
    
        new_player = copy.deepcopy(self)
        current_batters_faced = new_player.stats["BFP"]
        return new_player.multiply_stats(new_batters_faced / current_batters_faced)
    
    def copy(self):
        return copy.deepcopy(self)
    

    
    
