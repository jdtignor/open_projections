function showBatting() {
  document.getElementById('btnDownloadHitting').style.display = 'inline-block';
  document.getElementById('btnDownloadPitching').style.display = 'none'; 
  
  document.getElementById('divBatting').style.display = 'block';
  document.getElementById('divPitching').style.display = 'none';

  document.getElementById('tabBatting').classList.add("is-active")
  document.getElementById('tabPitching').classList.remove("is-active")

}

function showPitching() {
  document.getElementById('btnDownloadHitting').style.display = 'none';
  document.getElementById('btnDownloadPitching').style.display = 'inline-block';
  
  document.getElementById('divBatting').style.display = 'none';
  document.getElementById('divPitching').style.display = 'block'; 
  
  document.getElementById('tabBatting').classList.remove("is-active")
  document.getElementById('tabPitching').classList.add("is-active")        

}

function arrayToTable(tableData, tableID) {

  let table = document.getElementById(tableID);
  table.innerHTML = '';
  
  let headData = tableData.shift();
  let thead = document.createElement('thead');
  let tr = document.createElement('tr');
  
  for (let cellData of headData) {
    let th = document.createElement('th');
    if (cellData == 'playerID' || cellData == 'playerName') {
      th.classList.add('js-sort-string');
    }
    else {
      th.classList.add('js-sort-number')
    }
    th.innerHTML = cellData;
    tr.appendChild(th);
  }
  thead.appendChild(tr);
  table.appendChild(thead);
  
  let tbody = document.createElement('tbody');
  
  for (rowData of tableData) {
    if (rowData != '') {
      tr = document.createElement('tr')
      for (let cellData of rowData) {
        let td = document.createElement('td');
        td.innerHTML = cellData;
        tr.appendChild(td);
      }
      tbody.appendChild(tr);          
    }

  }
  table.appendChild(tbody);
  sortTable.init();

}

function updateTables() {

  var year = document.getElementById('selectYear').value;
  var version = document.getElementById('selectVersion').value;
  
  var fileName = "projections/" + version + "/" + year + "Batting" + version + ".csv";
  var hittingRequest = new XMLHttpRequest();
  
  hittingRequest.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      arrayToTable(Papa.parse(this.responseText).data, "batting-stats");
    }
  };
  
  hittingRequest.open("GET", fileName, true);
  hittingRequest.send();
  document.getElementById('btnDownloadHitting').href = fileName;
  
  fileName = "projections/" + version + "/" + year + "Pitching" + version + ".csv";
  var pitchingRequest = new XMLHttpRequest();
  
  pitchingRequest.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      arrayToTable(Papa.parse(this.responseText).data, "pitching-stats");
    }
  };
  
  pitchingRequest.open("GET", fileName, true);
  pitchingRequest.send();
  document.getElementById('btnDownloadPitching').href = fileName;
  
  document.getElementById('headingTxt').innerHTML = year + " Open Projections (" + version + ")";
}

select = document.getElementById('selectYear');
for (let i = 1980; i <= 2020; i++) {
    
  let year = 2019;
  let version = 'Aparicio';
  let option = document.createElement('option');
  
  option.value = i;
  option.innerHTML = i;
  
  if (i == year) {
  option.selected = true;
  }
  select.appendChild(option);
}

window.onload = function() {
  document.getElementById("tabBatting").onclick = function() {
    showBatting();
    return false;
  }
    
  document.getElementById("tabPitching").onclick = function() {
    showPitching();
    return false;
  }
}

updateTables();