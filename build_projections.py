import aparicio_batting
import aparicio_pitching
import blyleven_batting
import blyleven_pitching
import baseball_io

import numpy
import scipy.stats

start_year = 2020
final_year = 2020

def get_batting_correlation(stat, projected_hitters, actual_hitters, pa_minimum):
    targets = []
    projects = []
    
    for actual_hitter in actual_hitters.values():
        plate_appearances = actual_hitter.get_pa()
        player_id = actual_hitter.player_id
        
        if plate_appearances >= pa_minimum:
            if player_id in projected_hitters:
                targets.append(actual_hitter.stats[stat])
                projects.append(projected_hitters[player_id].stats[stat])
    
    r, p = scipy.stats.pearsonr(numpy.array(targets), numpy.array(projects))
    return r 

def get_pitching_correlation(stat, projected_pitchers, actual_pitchers, bfp_minimum):
    targets = []
    projects = []
    
    for actual_pitcher in actual_pitchers.values():
        player_id = actual_pitcher.player_id
        
        if actual_pitcher.stats["BFP"] >= bfp_minimum:
            if player_id in projected_pitchers:
                targets.append(actual_pitcher.stats[stat] / actual_pitcher.stats["BFP"])
                projects.append(projected_pitchers[player_id].stats[stat] / projected_pitchers[player_id].stats["BFP"])
    
    r, p = scipy.stats.pearsonr(numpy.array(targets), numpy.array(projects))
    return r 

    
def get_ops_correlation(projected_hitters, actual_hitters, pa_minimum):
    
    targets = []
    projects = []
    
    for actual_hitter in actual_hitters.values():
        plate_appearances = actual_hitter.get_pa()
        player_id = actual_hitter.player_id
        
        if plate_appearances >= pa_minimum:
            if player_id in projected_hitters:
                target_ops = actual_hitter.get_obp() + actual_hitter.get_slg()
                projected_ops = projected_hitters[player_id].get_obp() + projected_hitters[player_id].get_slg()
                targets.append(target_ops)
                projects.append(projected_ops)
                
                #print(player_id + " " + str(target_ops) + " " + str(projected_ops))
    
    r, p = scipy.stats.pearsonr(numpy.array(targets), numpy.array(projects))
    return r
    
def get_era_correlation(projected_pitchers, actual_pitchers, bfp_minimum):
    
    targets = []
    projects = []
    
    for actual_pitcher in actual_pitchers.values():
        player_id = actual_pitcher["playerID"]
        
        if actual_pitcher["BFP"] >= bfp_minimum:
            if player_id in projected_pitchers:
                target_era = actual_pitcher.get_era()
                projected_era = projected_pitchers[player_id].get_era()
                targets.append(target_era)
                projects.append(projected_era)
                
                #print(player_id + " " + str(target_era) + " " + str(projected_era))
    
    r, p = scipy.stats.pearsonr(numpy.array(targets), numpy.array(projects))
    return r

for year in range(start_year, final_year + 1):
    
    projected_hitters1 = aparicio_batting.build_batting_projections(year)
    projected_hitters2 = blyleven_batting.build_batting_projections(year)
    projected_pitchers1 = aparicio_pitching.build_pitching_projections(year)
    projected_pitchers2 = blyleven_pitching.build_pitching_projections(year)

    #actual_hitters = baseball_io.load_batting_stats(year)
    #actual_pitchers = baseball_io.load_pitching_stats(year) 
    
    #batting_correlation1 = get_stat_correlation("BB", projected_hitters1, actual_hitters, 500)
    #batting_correlation2 = get_stat_correlation("BB", projected_hitters2, actual_hitters, 500)
    
    #batting_correlation1 = get_ops_correlation(projected_hitters1, actual_hitters, 400)
    #batting_correlation2 = get_ops_correlation(projected_hitters2, actual_hitters, 400)
    #print(str(year) + " " + str(batting_correlation1) + " " + str(batting_correlation2))
        
   # pitching_correlation1 = get_pitching_correlation("ER", projected_pitchers1, actual_pitchers, 250)
    #pitching_correlation2 = get_pitching_correlation("ER", projected_pitchers2, actual_pitchers, 250)
    #print(str(year) + " " + str(pitching_correlation1) + " " + str(pitching_correlation2))
    
    
    