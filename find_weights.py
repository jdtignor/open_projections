import baseball_io
from player import Player
from sklearn.linear_model import LinearRegression

        
def load_data(year, stat, is_hitter):
    
    if is_hitter:
        year_stats = baseball_io.load_batting_stats(year)
        year1_stats = baseball_io.load_batting_stats(year - 1)
        year2_stats = baseball_io.load_batting_stats(year - 2)
        year3_stats = baseball_io.load_batting_stats(year - 3)
    else:
        year_stats = baseball_io.load_pitching_stats(year)
        year1_stats = baseball_io.load_pitching_stats(year - 1)
        year2_stats = baseball_io.load_pitching_stats(year - 2)
        year3_stats = baseball_io.load_pitching_stats(year - 3)
    
    x = []
    y = []
    
    for player_id, player in year_stats.items():

        if (player_id in year1_stats and
            player_id in year2_stats and       
            player_id in year3_stats):
            
            # Y is the result (the present year)
            if is_hitter:
                y.append(player.stats[stat] / player.get_pa())
                
                past_years = []            
                past_years.append(year1_stats[player_id].stats[stat] / year1_stats[player_id].get_pa())
                past_years.append(year2_stats[player_id].stats[stat] / year2_stats[player_id].get_pa()) 
                past_years.append(year3_stats[player_id].stats[stat] / year3_stats[player_id].get_pa())
                
                # X is the data we have from the previous 3 seasons
                x.append(past_years)
            else:
                y.append(player.stats[stat] / player.stats["BFP"])
                
                past_years = []            
                past_years.append(year1_stats[player_id].stats[stat] / year1_stats[player_id].stats["BFP"])
                past_years.append(year2_stats[player_id].stats[stat] / year2_stats[player_id].stats["BFP"]) 
                past_years.append(year3_stats[player_id].stats[stat] / year3_stats[player_id].stats["BFP"])
                
                # X is the data we have from the previous 3 seasons
                x.append(past_years)

        
    return x, y
    
 
def find_weights(stat_name, is_hitter):
    
    x = []
    y = []
    
    for year in range(2000, 2020):
        
        year_x, year_y = load_data(year, stat_name, is_hitter)
        x = x + year_x
        y = y + year_y
        
    model = LinearRegression().fit(x, y)

    print(model.coef_)
    print(model.score(x,y))



find_weights("SO", False)

