from player import Player

def get_average_hitter(hitters, minimum_pa):

    average_player = Player(True)
    player_count = 0
    
    for player in hitters.values():
    
        if player.get_pa() >= minimum_pa:
        
            for stat in Player.batting_stats:
                average_player.stats[stat] += player.stats[stat]
                player_count += 1

    
    for stat in Player.batting_stats:
        average_player.stats[stat] /= player_count

    return average_player
    
    
def get_average_pitcher(pitchers, minimum_bfp):

    average_player = Player(False)
    player_count = 0
    
    for player in pitchers.values():
     
        if player.stats["BFP"] >= minimum_bfp:
        
            for stat in Player.pitching_stats:
                average_player.stats[stat] += player.stats[stat]
                player_count += 1

    
    for stat in Player.pitching_stats:
        average_player.stats[stat] /= player_count

    return average_player

    
def calculate_weighted_rate(values, denominators, weights):

    sum_of_values = 0
    sum_of_weights = 0
    sum_of_denominators = 0
    
    for value, weight, denominator in zip(values, weights, denominators):
        if value is not None:
            sum_of_values += (value * weight)
            sum_of_denominators += (denominator * weight)
            sum_of_weights += weight

    return (sum_of_values / sum_of_weights) / (sum_of_denominators / sum_of_weights)