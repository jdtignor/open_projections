import csv
import os
from player import Player

def output_batting_projections(year, version, projected_players):
    
    path = "public/projections/" + str(version)
    if not os.path.exists(path):
        os.makedirs(path)
    
    with open(path + "/" + str(year) + "Batting" + version + ".csv", "w", newline="") as csvFile:
        
        playerWriter = csv.writer(csvFile)
        
        columns = ["playerID", "playerName"] + Player.batting_stats
        
        # First output the header row
        playerWriter.writerow(columns)

        # Then output each player's stats
        for player in projected_players.values():
            
            data = []
            data.append(player.player_id)
            data.append(player.name)
            for stat in Player.batting_stats:
                data.append(player.stats[stat])
                
            playerWriter.writerow(data)

def output_pitching_projections(year, version, projected_stats):
    
    path = "public/projections/" + str(version)
    if not os.path.exists(path):
        os.makedirs(path)
    
    with open(path + "/" + str(year) + "Pitching" + version + ".csv", "w", newline="") as csvFile:
        
        playerWriter = csv.writer(csvFile)
        
        columns = ["playerID", "playerName"] + Player.pitching_stats
        
        # First output the header row
        playerWriter.writerow(columns)

        # Then output each player's stats
        for player in projected_stats.values():
            
            data = []
            data.append(player.player_id)
            data.append(player.name)
            for stat in Player.pitching_stats:
                data.append(player.stats[stat])
                
            playerWriter.writerow(data)


def load_batting_stats(year):

    hitters = {}
    
    # We'll get a list of pitchers to cull out pitchers' hitting stats
    pitcher_ids = get_pitcher_ids(year)
    
    with open("public/data/baseballdatabank-master/core/Batting.csv", "r") as csvFile:
        playerReader = csv.DictReader(csvFile)
        
        for row in playerReader:
            if (int(row["yearID"]) == year and 
                int(row["H"]) > 0 and
                row["playerID"] not in pitcher_ids):
                
                player_id = row["playerID"]
                # Baseball Databank has multiple rows for players who played for 
                # more than one team in a given season. If this is the first row,
                # we'll add the player to our array. Otherwise, we'll increment his stats.
                if player_id not in hitters:
                    hitters[player_id] = Player(True)        
                    hitters[player_id].player_id = row["playerID"]
                    hitters[player_id].year = row["yearID"]
                    for stat in Player.batting_stats:
                        if row[stat]:
                            hitters[player_id].stats[stat] = int(row[stat])
                        else:
                            hitters[player_id].stats[stat] = 0
                else:
                    for stat in Player.batting_stats:
                        if row[stat]:
                            hitters[player_id].stats[stat] = int(row[stat])
                        else:
                            hitters[player_id].stats[stat] = 0
        

    player_ages = get_player_ages(year)
    
    for player_id, hitter in hitters.items():
        hitter.age = player_ages[player_id]
        
    player_names = get_player_names(year)
    
    for player_id, hitter in hitters.items():
        hitter.name = player_names[player_id]

    return hitters

def load_pitching_stats(year):

    pitchers = {}
    
    
    # We'll get a list of pitcher ids to cull out position players pitching
    pitcher_ids = get_pitcher_ids(year)

    with open("public/data/baseballdatabank-master/core/Pitching.csv", "r") as csvFile:
        playerReader = csv.DictReader(csvFile)
        
        for row in playerReader:
            if (int(row["yearID"]) == year and
                row["playerID"] in pitcher_ids):
                
                player_id = row["playerID"]
                # Baseball Databank has multiple rows for players who played for 
                # more than one team in a given season. If this is the first row,
                # we'll add the player to our array. Otherwise, we'll increment his stats.
                if player_id not in pitchers:
                    pitchers[player_id] = Player(False)
                    pitchers[player_id].player_id = row["playerID"]
                    pitchers[player_id].year = row["yearID"]
                    for stat in Player.pitching_stats:
                        if row[stat]:
                            pitchers[player_id].stats[stat] = int(row[stat])
                        else:
                            pitchers[player_id].stats[stat] = 0
                else:
                    for stat in Player.pitching_stats:
                        if row[stat]:
                            pitchers[player_id].stats[stat] = int(row[stat])
                        else:
                            pitchers[player_id].stats[stat] = 0
        

    player_ages = get_player_ages(year)
    
    for player_id, pitcher in pitchers.items():
        pitcher.age = player_ages[player_id]
        
    player_names = get_player_names(year)
    
    for player_id, pitcher in pitchers.items():
        pitcher.name = player_names[player_id]

    return pitchers

# Right now this opens the file to get ages for each year...
# It would be a bit more efficient to get the birth years once (static variable?)
def get_player_ages(year):

    player_ages = {}

    with open("public/data/baseballdatabank-master/core/People.csv", "r") as csvFile:
        playerReader = csv.DictReader(csvFile)
        
        for row in playerReader:
            if row["birthYear"] != "":
                player_ages[row["playerID"]] = year - int(row["birthYear"])
    
    return player_ages

def get_player_names(year):

    player_names = {}

    with open("public/data/baseballdatabank-master/core/People.csv", "r") as csvFile:
        playerReader = csv.DictReader(csvFile)
        
        for row in playerReader:
            player_names[row["playerID"]] = row["nameFirst"] + " " + row["nameLast"]
    
    return player_names

def get_pitcher_ids(year):

    pitchers = []

    with open("public/data/baseballdatabank-master/core/Appearances.csv", "r") as csvFile:
        playerReader = csv.DictReader(csvFile)
        
        for row in playerReader:
            if int(row["yearID"]) == year:
                # Our standard for pitcher is a player with > 50% of
                # appearances as a P
                if (int(row["G_p"]) * 2) > int(row["G_all"]):
                    pitchers.append(row["playerID"]) 
    
    return pitchers