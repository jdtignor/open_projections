from player import Player
import projection_calc
import baseball_io

VERSION_NAME = "Blyleven"

# Weights are YEAR - 1, YEAR - 2, YEAR - 3, and LEAGUE AVERAGE
WEIGHTS = {}
WEIGHTS["SO"] = [1.0, 0.4, 0.3, 0.1]
WEIGHTS["BB"] = [1.0, 0.6, 0.4, 0.1]
WEIGHTS["HR"] = [1.0, 0.6, 0.5, 0.2]
WEIGHTS["H"] = [1.0, 0.7, 0.6, 1.0]
BASE_WEIGHTS = [5, 4, 3, 2]

YEAR1_PA_WEIGHT = 0.5
YEAR2_PA_WEIGHT = 0.1
PA_BASE = 200

year1_stats = {}
year2_stats = {}
year3_stats = {}
average_player = {}

def build_3year_array(player_id, stat):

    stat_array = []
    
    if stat == "PA":
        if player_id in year1_stats:
            stat_array.append(year1_stats[player_id].get_pa())
        else: stat_array.append(None)
        
        if player_id in year2_stats: 
            stat_array.append(year2_stats[player_id].get_pa())
        else: stat_array.append(None)
        
        if player_id in year3_stats: 
            stat_array.append(year3_stats[player_id].get_pa())
        else: stat_array.append(None)

        stat_array.append(average_player.get_pa())
    else:
        if player_id in year1_stats: 
            stat_array.append(year1_stats[player_id].stats[stat])
        else: stat_array.append(None)
        
        if player_id in year2_stats: 
            stat_array.append(year2_stats[player_id].stats[stat])
        else: stat_array.append(None)
        
        if player_id in year3_stats: 
            stat_array.append(year3_stats[player_id].stats[stat])
        else: stat_array.append(None)
        
        stat_array.append(average_player.stats[stat])
        
    return stat_array

def build_batting_projections(year):

    global year1_stats
    global year2_stats
    global year3_stats
    global average_player
    
    year1_stats = baseball_io.load_batting_stats(year - 1)
    year2_stats = baseball_io.load_batting_stats(year - 2)
    year3_stats = baseball_io.load_batting_stats(year - 3)
    
    # We need to get league average stats to regress to the mean, scaled to
    # 600 PA
    average_player = projection_calc.get_average_hitter(year1_stats, 200)
    average_player = average_player.adjust_player_pa(600)
    
    # Now we're ready to project, let's start with a list of players from the
    # previous season
    projected_players = {}
    for player_id, player in year1_stats.items():
        
        projected_player = Player(True)
        projected_player.player_id = player_id
        projected_player.name = player.name
        projected_player.year = player.year
        projected_player.age = player.age + 1

        # Get list of PA to pass to for each component projection
        plate_appearances = build_3year_array(player_id, "PA")
        
        # Project BABIP
        hits = build_3year_array(player_id, "H")
        projected_hit_rate = projection_calc.calculate_weighted_rate(hits, plate_appearances, WEIGHTS["H"])

        # Project strikeouts
        strikeouts = build_3year_array(player_id, "SO")
        projected_so_rate = projection_calc.calculate_weighted_rate(strikeouts, plate_appearances, WEIGHTS["SO"])

        # Project walks
        walks = build_3year_array(player_id, "BB")
        projected_bb_rate = projection_calc.calculate_weighted_rate(walks, plate_appearances, WEIGHTS["BB"])
        
        # Project homeruns
        homeruns = build_3year_array(player_id, "HR")
        projected_hr_rate = projection_calc.calculate_weighted_rate(homeruns, plate_appearances, WEIGHTS["HR"])
        
        # Project everything else

        # First add in last year's stats, weighted appropriately
        projected_player.add_stats(year1_stats[player_id].multiply_stats(BASE_WEIGHTS[0]))
        denominator = BASE_WEIGHTS[0]

        # Factor in two years ago, weighted appropriately
        if player_id in year2_stats:
            projected_player.add_stats(year2_stats[player_id].multiply_stats(BASE_WEIGHTS[1]))
            denominator += BASE_WEIGHTS[1]

        # Factor in three years ago, weighted appropriately
        if player_id in year3_stats:
            projected_player.add_stats(year3_stats[player_id].multiply_stats(BASE_WEIGHTS[2]))
            denominator += BASE_WEIGHTS[2]
        
        # Regress to the mean
        projected_player.add_stats(average_player.multiply_stats(BASE_WEIGHTS[3]))
        denominator += BASE_WEIGHTS[3]
        
        # Divide by the total weights to give the weighted average
        projected_player = projected_player.divide_stats(denominator)
        
        # Save 2B and 3B frequency
        double_rate = projected_player.stats["2B"] / projected_player.stats["H"]
        triple_rate = projected_player.stats["3B"] / projected_player.stats["H"]
        
        # Replace the base rates with custom calculations
        projected_player.stats["SO"] = projected_so_rate * projected_player.get_pa()
        projected_player.stats["BB"] = projected_bb_rate * projected_player.get_pa()
        projected_player.stats["HR"] = projected_hr_rate * projected_player.get_pa()
        projected_player.stats["H"] = projected_hit_rate * projected_player.get_pa()
        
        # Adjust 2B and 3B for new hit rate
        projected_player.stats["2B"] = projected_player.stats["H"] * double_rate
        projected_player.stats["3B"] = projected_player.stats["H"] * triple_rate
        
        # Adjust playing time
        projected_pa = PA_BASE
        if player_id in year1_stats: 
            projected_pa += year1_stats[player_id].get_pa() * YEAR1_PA_WEIGHT
        if player_id in year2_stats: 
            projected_pa += year2_stats[player_id].get_pa() * YEAR2_PA_WEIGHT
        
        projected_player = projected_player.multiply_stats(projected_pa / projected_player.get_pa())
        
        # Adjust for age
        age_adjustment = calculate_age_adjustment(projected_player.age)
        projected_player = projected_player.multiply_stats(age_adjustment)
        
        # Now round everything off to whole numbers
        projected_player = projected_player.round_stats()
        
        # Add this to our dictionary of finished projections
        projected_players[player_id] = projected_player

    baseball_io.output_batting_projections(year, VERSION_NAME, projected_players)
    
    return projected_players


def calculate_age_adjustment(age):

    # Is he older than 29?
    if age > 29:
        # Should be a negative adjustment
        return 1 + ((29 - age) * 0.003)
    elif age < 29:
        # Should be a positive adjustment
        return 1 + ((29 - age) * 0.006)
    else:
        return 1

