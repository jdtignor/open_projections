from player import Player
import projection_calc
import baseball_io

VERSION_NAME = "Blyleven"

# Weights are YEAR - 1, YEAR - 2, YEAR - 3, and LEAGUE AVERAGE
WEIGHTS = {}
WEIGHTS["SO"] = [1.0, 0.6, 0.4, 0.0]
WEIGHTS["BB"] = [1.0, 0.6, 0.6, 0.3]
WEIGHTS["HR"] = [1.0, 0.7, 0.4, 0.9]
WEIGHTS["H"]  = [1.0, 0.8, 0.4, 0.3]
WEIGHTS["ER"] = [1.0, 0.6, 0.1, 1.0]
BASE_WEIGHTS = [3, 2, 1, 3]

YEAR1_IP_WEIGHT = 0.5
YEAR2_IP_WEIGHT = 0.1
IP_BASE_SP = 60
IP_BASE_RP = 25

year1_stats = {}
year2_stats = {}
year3_stats = {}
average_player = {}

def build_3year_array(player_id, stat):

    stat_array = []
    
    if player_id in year1_stats: 
        stat_array.append(year1_stats[player_id].stats[stat])
    else: stat_array.append(None)
    
    if player_id in year2_stats: 
        stat_array.append(year2_stats[player_id].stats[stat])
    else: stat_array.append(None)
    
    if player_id in year3_stats: 
        stat_array.append(year3_stats[player_id].stats[stat])
    else: stat_array.append(None)
    
    stat_array.append(average_player.stats[stat])
        
    return stat_array

def build_pitching_projections(year):

    global year1_stats
    global year2_stats
    global year3_stats
    global average_player
    
    year1_stats = baseball_io.load_pitching_stats(year - 1)
    year2_stats = baseball_io.load_pitching_stats(year - 2)
    year3_stats = baseball_io.load_pitching_stats(year - 3)
    
    
    # We need to get league average stats to regress to the mean, scaled to
    # 600 batters faced.
    average_player = projection_calc.get_average_pitcher(year1_stats, 0)
    average_player = average_player.adjust_player_bfp(600)
    
    # Now we're ready to project, let's start with a list of players from the
    # previous season
    projected_players = {}
    for player_id, player in year1_stats.items():
        
        projected_player = Player(False)
        projected_player.player_id = player_id
        projected_player.name = player.name
        projected_player.year = player.year
        projected_player.age = player.age + 1
        
        player_bfp = build_3year_array(player_id, "BFP")
        for stat_name in Player.pitching_stats:
            player_stat = build_3year_array(player_id, stat_name)
            if stat_name in WEIGHTS:
                weights = WEIGHTS[stat_name]
            else:
                weights = BASE_WEIGHTS
            stat_rate = projection_calc.calculate_weighted_rate(player_stat, player_bfp, weights)
            projected_player.stats[stat_name] = stat_rate * year1_stats[player_id].stats["BFP"]
        
        # Adjust playing time
        # First, consider if they are a SP or RP
        if player_id in year1_stats:
            start_rate = year1_stats[player_id].stats["GS"] / year1_stats[player_id].stats["G"]
        else:
            start_rate = 0
        role_ip_diff = IP_BASE_SP - IP_BASE_RP
        projected_outs = (IP_BASE_RP * 3) + (start_rate * role_ip_diff * 3)
        
        if player_id in year1_stats: 
            projected_outs += year1_stats[player_id].stats["IPouts"] * YEAR1_IP_WEIGHT
        if player_id in year2_stats: 
            projected_outs += year2_stats[player_id].stats["IPouts"] * YEAR2_IP_WEIGHT
        
        projected_player = projected_player.multiply_stats(projected_outs / projected_player.stats["IPouts"])
        
        # Adjust for age
        age_adjustment = calculate_age_adjustment(projected_player.age)
        projected_player = projected_player.multiply_stats(age_adjustment)
        
        # Now round everything off to whole numbers
        projected_player = projected_player.round_stats()
        
        projected_players[player_id] = projected_player

    baseball_io.output_pitching_projections(year, VERSION_NAME, projected_players)
    
    return projected_players


def calculate_age_adjustment(age):

    # Is he older than 29?
    if age > 29:
        # Should be a negative adjustment
        return 1 + ((29 - age) * 0.003)
    elif age < 29:
        # Should be a positive adjustment
        return 1 + ((29 - age) * 0.006)
    else:
        return 1
